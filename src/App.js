import React from 'react';
import ReactDOM from 'react-dom';
import './App.css'

function App() {
  return (
    <div>
      <h1>Hello, world!</h1>
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));

export default App;